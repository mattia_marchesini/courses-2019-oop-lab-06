package it.unibo.oop.lab.collections1;

import java.util.*;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

	private UseCollection() {
	}

	/**
	 * @param s unused
	 */
	public static void main(final String... s) {
		/*
		 * 1) Create a new ArrayList<Integer>, and populate it with the numbers from
		 * 1000 (included) to 2000 (excluded).
		 */
		ArrayList<Integer> list = new ArrayList<>();
		for (int i = 1000; i < 2000; i++) {
			list.add(i);
		}

		/*
		 * 2) Create a new LinkedList<Integer> and, in a single line of code without
		 * using any looping construct (for, while), populate it with the same contents
		 * of the list of point 1.
		 */
		LinkedList<Integer> list2 = new LinkedList<>(list);

		/*
		 * 3) Using "set" and "get" and "size" methods, swap the first and last element
		 * of the first list. You can not use any "magic number". (Suggestion: use a
		 * temporary variable)
		 */
		final int LAST = list.size() - 1;
		final int first_element = list.get(0);
		list.set(0, list.get(LAST));
		list.set(LAST, first_element);

		/*
		 * 4) Using a single for-each, print the contents of the Arraylist.
		 */
		for (final int i : list) {
			System.out.print(i + " ");
		}
		System.out.println();

		/*
		 * 5) Measure the performance of inserting new elements in the head of the
		 * collection: measure the time required to add 100.000 elements as first
		 * element of the collection for both ArrayList and LinkedList, using the
		 * previous lists. In order to measure times, use as example
		 * TestPerformance.java.
		 */
		final int ELEMS = 100_000;
		long time = System.nanoTime();
		for (int i = 1; i <= ELEMS; i++) {
			list.add(i);
		}
		time = System.nanoTime() - time;
		System.out.println("the time for ArrayList is: " + time);

		time = System.nanoTime();
		for (int i = 1; i <= ELEMS; i++) {
			list2.add(i);
		}
		time = System.nanoTime() - time;
		System.out.println("the time for LinkedList is: " + time);

		/*
		 * 6) Measure the performance of reading 1000 times an element whose position is
		 * in the middle of the collection for both ArrayList and LinkedList, using the
		 * collections of point 5. In order to measure times, use as example
		 * TestPerformance.java.
		 */
		time = System.nanoTime();
		for (int i = 1; i <= ELEMS; i++) {
			list.add(i);
		}
		time = System.nanoTime() - time;
		System.out.println("the time is: " + time);
		/*
		 * 7) Build a new Map that associates to each continent's name its population:
		 * 
		 * Africa -> 1,110,635,000
		 * 
		 * Americas -> 972,005,000
		 * 
		 * Antarctica -> 0
		 * 
		 * Asia -> 4,298,723,000
		 * 
		 * Europe -> 742,452,000
		 * 
		 * Oceania -> 38,304,000
		 */
		/*
		 * 8) Compute the population of the world
		 */
	}
}